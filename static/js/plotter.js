function get_hw(dataset) {
    var hardwares = [];
    for (var hardware in dataset) {
        var lin_hardware = hardware.replace("win","");
        if (! hardwares.includes(lin_hardware)) {
            hardwares.push(lin_hardware);
        }
    }
    return hardwares;
}

function do_plot(placeholder_id, click_id, dataset) {
    var data = []
    var hardwares = get_hw(dataset);
    var colors = ["#edc240", "#236AB9", "#cb4b4b", "#4da74d", "#9440ed",
        "#154360", "#FC7307", "#341C09", "#4424D6", "#FCCB1A", "#091D34",
        "#edc240", "#236AB9", "#cb4b4b", "#4da74d", "#9440ed", "#154360",
        "#FC7307", "#341C09", "#4424D6", "#FCCB1A", "#091D34"];
    var len = hardwares.length;
    var ymax = 0;

    // add platform checkboxes
    var choiceContainer = $(placeholder_id + "_hw_choices");
    $.each(dataset, function(key, val) {
        choiceContainer.append("<br/>" +
            "<input style='display: inline-block;' type='checkbox' name='" + placeholder_id + '_' + key +
            "' checked='checked' id='" + placeholder_id + '_' + key + "_checkbox'></input>" +
            "<label style='display: inline-block;' for='" + placeholder_id + '_' + key + "_checkbox'>"
            + key + "<div style='display: inline-block; width: 20px; height: 5px; background: " +
            colors[hardwares.indexOf(key)] + "; margin:3px;' /></label>"
        );
    });
    choiceContainer.find("input").click(plotAccordingToChoices);

    function plotAccordingToChoices() {
        var data = [];
        choiceContainer.find("input:checked").each(function () {
            var hardware = $(this).attr("name").replace(placeholder_id + '_', '');
            if (hardware && dataset[hardware]) {
                var color_idx = hardwares.indexOf(hardware);
                var data_points = {
                    errorbars: "y",
                    show: true,
                    yerr: {show:true, upperCap: "-", lowerCap: "-"},
                    lineWidth: 3,
                    fillColor: colors[color_idx],
                }

                var d1 = {
                    label: hardware,
                    data: [],
                    points: data_points,
                    color: colors[color_idx],
                };
                for (var build in dataset[hardware]){
                    var build_data = dataset[hardware][build];
                    var score = build_data["score"];
                    var deviation = build_data["deviation"];
                    var point = [build_data["mesa_date"], score];
                    if (deviation > 0.05) {
                        point.push(deviation);
                    } else {
                        point.push(0);
                    }
                    d1.data.push(point);
                    if (score + deviation > ymax) {
                        ymax = score + deviation;
                    }
                }
                // Make sure build points are sorted by date
                d1.data.sort(function(a, b) {
                    return a[0] < b[0];
                });
                data.push(d1);
            }
        });
        ymax = Math.round(ymax * 10.0 + 0.5) / 10.0;
        if (data.length > 0) {

            $.plot(placeholder_id, data, {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 3,
                    },
                    points: {
                        show:true,
                    },
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    //autoHightlight: true,
                    borderWidth: 0,
                    margin: {
                        bottom: 20,
                    },
                },
                yaxis: {
                    min: 0.0,
                    max: ymax,
                    zoomRange: false
                },
                xaxis: {
                    mode: "time",
                    minTickSize: [1, "day"],
                    show: true,
                },
                zoom: {
                    interactive: true
                },
                pan: {
                    interactive: true
                }
            });
        }
    }

    plotAccordingToChoices();


    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid ',
            padding: '2px',
            'background-color': '#fff'
        }).appendTo("body").fadeIn(200);
    }
    var previousPoint = null;
    $(placeholder_id).bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.seriesIndex) {
                previousPoint = item.seriesIndex;

                $("#tooltip").remove();
                //var x = item.datapoint[0].toFixed(0),
                //y = item.datapoint[1].toFixed(0);

                var curr_sha = dataset[item.series.label][item.dataIndex]["commit"].slice(5);
                showTooltip(item.pageX, item.pageY,
                    '<div><table><tr><td/><td/></tr>' +
                    '<tr><td>Hardware: </td>' +
                    '<td>' + item.series.label + '</td></tr>' +
                    '<tr><td>Kernel: </td>' +
                    '<td>' + dataset[item.series.label][item.dataIndex]["kernel"] + '</td></tr>' +
                    '<tr><td>Score: </td>' +
                    '<td>' + (Math.round(dataset[item.series.label][item.dataIndex]["score"] * 1000) / 1000) + '</td></tr></table></div>'
                );
            }
        }
        else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });

    /* This function will build some data below the graph, information about
     * the commit, standard deviation, the averaged score of the tests, and
     * will build buttons to build a newer and older sha.
     */

    $(placeholder_id).bind("plotclick", function (event, pos, item) {
        if (item) {
            // Only try to get a previous sha if we are not on the oldest sha already
            if (item.dataIndex !== 0) {
                var prev_sha = dataset[item.series.label][item.dataIndex - 1]["commit"].slice(5);
            } else {
                var prev_sha = '';
            }

            // The current sha is always valid
            var curr_sha = dataset[item.series.label][item.dataIndex]["commit"].slice(5);

            // Only try to get the next sha if we are not on the newest sha
            if (item.dataIndex !== (dataset[item.series.label].length - 1)) {
                var next_sha = dataset[item.series.label][item.dataIndex + 1]["commit"].slice(5);
            } else {
                var next_sha = '';
            }

            // Build the table of data and the raw html for the buttons
            $(click_id).html(
                '<div><table><tr><td/><td/></tr>' +
                '<tr><td>Hardware</td>' +
                '<td>' + item.series.label + '</td></tr>' +
                '<tr><td>Kernel</td>' +
                '<td>' + dataset[item.series.label][item.dataIndex]["kernel"] + '</td>' +
                '<tr><td>Commit</td>' +
                '<td><a href="https://cgit.freedesktop.org/mesa/mesa/commit/?id=' + curr_sha + '">' + curr_sha  + '</a></td>' +
                '<tr><td>Score</td>' +
                '<td>' + dataset[item.series.label][item.dataIndex]["score"] + '</td>' +
                '<tr><td>Standard Deviation</td>' +
                '<td>' + dataset[item.series.label][item.dataIndex]["deviation"] + '</td>' +
                '</tr></table></div>'
            );
        }
    });
}
