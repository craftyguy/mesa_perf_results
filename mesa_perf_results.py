import datetime
import urllib
from dateutil.relativedelta import relativedelta
from flask import (Flask, flash, make_response, redirect, render_template,
                   request)
import flask_compress
import os
from pony.orm import db_session, flush
from pony import orm

from models import db

app = Flask(__name__)
flask_compress.Compress(app)

secure_cookie = True
# Allow disabling the secure cookie flag (e.g. when running for development)
if 'SESSION_COOKIE_SECURE' in os.environ:
    secure_cookie = os.environ.get('SESSION_COOKIE_SECURE').lower()
    secure_cookie = secure_cookie == 'true' or secure_cookie == '1'
# Flag for indicating whether site is internal or not. Used to hide or
# display certain elements of the site that don't make sense if site is
# not internal
internal_site = False
if 'SITE_IS_INTERNAL' in os.environ:
    internal_site = os.environ.get('SITE_IS_INTERNAL').lower()
    internal_site = internal_site == 'true' or internal_site == '1'

# Initialize db object
sql_user = 'jenkins'
if "SQL_DATABASE_USER" in os.environ:
    sql_user = os.environ["SQL_DATABASE_USER"]

sql_pw = ''
if "SQL_DATABASE_PW_FILE" in os.environ:
    sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
    if os.path.exists(sql_pw_file):
        with open(sql_pw_file, 'r') as f:
            sql_pw = f.read().rstrip()
sql_host = "localhost"
if "SQL_DATABASE_HOST" in os.environ:
    sql_host = os.environ["SQL_DATABASE_HOST"]

print("************************** " + sql_pw)
if sql_pw:
    db.bind('mysql', host=sql_host, user=sql_user, password=sql_pw,
            database='mesa_perf_results')
else:
    db.bind('mysql', host=sql_host, user=sql_user,
            database='mesa_perf_results')
db.generate_mapping(create_tables=True)


def get_section_sort_prefs(request, sections):
    """ Read section sort preferences from cookies
    request: flask request object
    sections: list of sections on page
    Returns: dictionary of section sorting prefs, or None """
    sort_prefs = {}
    for section in sections:
        cookie = request.cookies.get(section + '_sort_pref')
        if not cookie:
            sort_prefs[section] = ''
        else:
            sort_prefs[section] = cookie
    return sort_prefs


def apply_new_section_expand(request, expand_prefs):
    """ Save new section expand preferences to cookie(s)
    request: flask request object
    expand_prefs: dictionary of section sorting prefs
    Returns: flask response with cookies set, or None """
    resp = None
    section = request.args.get('expand_toggle')
    if section in expand_prefs:
        expand_prefs[section] = expand_prefs[section] is not True
        resp = make_response(
            redirect(request.path
                     + '#' + urllib.parse.quote_plus('%s' % section), code=303))
        resp.set_cookie(section + '_expand', str(expand_prefs[section]),
                        expires=(datetime.datetime.now()
                                 + datetime.timedelta(days=9000)),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
    return resp


def get_section_expand_prefs(request, sections):
    """ Read section expand preferences from cookies
    request: flask request object
    sections: list of sections on page
    Returns: dictionary of section expand prefs, or None """
    expand_prefs = {}
    for section in sections:
        cookie = request.cookies.get(section + '_expand')
        if not cookie:
            expand_prefs[section] = False
        else:
            expand_prefs[section] = cookie.lower() == 'true'
    return expand_prefs


def apply_new_section_sort(request, sort_prefs, req_path=None):
    """ Save new section sorting preferences to cookie(s)
    request: flask request object
    sort_prefs: dictionary of section sorting prefs
    Returns: flask response with cookies set, or None """
    resp = None
    if not req_path:
        req_path = request.path
    new_sort_col = request.args.get('sort_col')
    new_sort_section = request.args.get('sort_section')
    if new_sort_section in sort_prefs:
        direction = 'asc'
        if sort_prefs[new_sort_section]:
            try:
                old_col, old_direction = sort_prefs[new_sort_section].split(':')
            except ValueError:
                # Unable to unpack old sort preferences, so default to sorting
                # by new col and asc direction
                old_col, old_direction = (new_sort_col, 'asc')
            if new_sort_col == old_col:
                # Use opposite direction if section is already sorted by column
                if old_direction == 'asc':
                    direction = 'desc'
        # Save new sort preferences and redirect to clear querystring
        resp = make_response(
            redirect(request.path +
                     '#' + urllib.parse.quote_plus('%s' % new_sort_section),
                     code=303))
        resp.set_cookie(new_sort_section + '_sort_pref',
                        '%s:%s' % (new_sort_col, direction),
                        expires=(datetime.datetime.now()
                                 + datetime.timedelta(days=9000)),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
    return resp


def sort_section(section_dicts, sort_pref):
    """ Sort given section by column/direction
    section_dicts: list of dictionaries (representing rows)
    sort_pref: string in the form of '<column>:<direction>'
    Returns: sorted section_dicts or section_dicts if unable to sort """
    if not section_dicts:
        return []
    if sort_pref and ':' in sort_pref:
        try:
            col, direction = sort_pref.split(':')
        except ValueError:
            return section_dicts
        if col in section_dicts[0]:
            return sorted(section_dicts, key=lambda k: k[col],
                          reverse=direction == 'desc')
    return section_dicts


@app.route("/")
@db_session
def main_page():
    sections = ['jobs']
    jobfilters = []
    jobfilter_str = ''
    # Apply any job filters if included in query string
    if 'jobfilter' in request.args:
        jobfilter_str = request.args.get('jobfilter')
        resp = make_response(redirect(request.path, code=303))
        resp.set_cookie('jobfilter', jobfilter_str,
                        expires=datetime.datetime.now() +
                        datetime.timedelta(days=9000),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
        return resp

    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs)
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp
    # Get job filter preference
    jobfilter_str = request.cookies.get('jobfilter')
    if jobfilter_str:
        # Delimiter for terms is whitespace
        jobfilters = jobfilter_str.split()

    job_dicts = []
    jobs = orm.select(j for j in db.Job)[:]
    for job in jobs:
        builds = orm.select(b for b in db.Build
                            if b.job.id == job.id
                            ).sort_by(db.Build.build_date)[:]
        last_build_id = builds[0].build_id
        last_build_name = builds[0].name
        last_build_time = builds[0].build_date

        job_dicts.append({
            'name': job.name,
            'last_build_id': str(last_build_id),
            'last_build_name': last_build_name,
            'last_build_time': str(last_build_time),
        })

    # filter jobs based on user preference
    filtered_job_dicts = []
    if jobfilters:
        for jobfilter in jobfilters:
            if jobfilter is None:
                continue
            for job in job_dicts:
                if jobfilter in job['name'] and job not in filtered_job_dicts:
                    filtered_job_dicts.append(job)
        job_dicts = filtered_job_dicts

    return render_template('main.html',
                           jobs=job_dicts,
                           top_links=[{"text": "Intel Mesa Performance CI",
                                       "href": "."}],
                           jobfilter=jobfilter_str, sort_prefs=sort_prefs)


@app.route("/<job_name>/builds/")
@db_session
def builds_page(job_name):
    # Get sort preferences, save preferences if new ones are passed by
    # query string
    sections = ['builds']
    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs)
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp
    job = orm.select(j for j in db.Job if j.name == job_name)[:][0]
    benchmarks = orm.select(b for b in db.Benchmark
                            if b.job.id == job.id)[:]
    benchmark_dicts = []
    for b in benchmarks:

        builds = orm.select(build for build in db.Build
                            if build.benchmark.id == b.id)[:]
        hardware = {}
        for build in builds:
            if build.hardware.hardware not in hardware:
                hardware[build.hardware.hardware] = []
            # Note: Values below that are casted to int, rounded, etc are done
            # to reduce the page size sent to the client
            hardware[build.hardware.hardware].append({
                'mesa_date': int(build.mesa_date.timestamp()),
                'score': round(build.score_avg, 2),
                'deviation': round(build.score_std, 4),
                'commit': "mesa=" + "TODO", #build.revisions.commit,
                'kernel': build.kernel,
            })

        benchmark_dicts.append({
            'name': b.name,
            'hardware': hardware
        })

    build_dicts = []
    return render_template('builds.html', job=job_name, builds=build_dicts,
                           benchmark_dicts=benchmark_dicts,
                           top_links=[{"text": "Intel Mesa Performance CI",
                                       "href": "../.."},
                                      {"text": job_name,
                                       "href": "."}],
                           title='Builds for ' + job_name,
                           sort_prefs=sort_prefs)
