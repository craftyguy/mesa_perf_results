import datetime
from pony import orm

db = orm.Database()


class Job(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    name = orm.Required(str, unique=True)
    benchmarks = orm.Set('Benchmark')
    builds = orm.Set('Build')
    hardware = orm.Set('Hardware')


class Benchmark(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    name = orm.Required(str)
    job = orm.Required(Job)
    builds = orm.Set('Build')


class Hardware(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    hostname = orm.Required(str)
    hardware = orm.Required(str)
    builds = orm.Set('Build')
    jobs = orm.Set('Job')


class Build(db.Entity):
    build_id = orm.Required(int)
    name = orm.Required(str)
    build_date = orm.Required(datetime.datetime)
    mesa_date = orm.Required(datetime.datetime)
    score_avg = orm.Required(float)
    score_std = orm.Required(float)
    kernel = orm.Required(str)
    revisions = orm.Set('Revision')
    benchmark = orm.Required(Benchmark)
    job = orm.Required(Job)
    hardware = orm.Required(Hardware)


class Revision(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    commit = orm.Required(str)
    author = orm.Required(str)
    description = orm.Required(str)
    sha = orm.Required(str)
    build = orm.Required(Build)
